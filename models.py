from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgres://postgres:bookhub@localhost:5433/bookhub')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):

    __tablename__ = 'book'

    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.String(80), primary_key = True)
    isbn = db.Column(db.String(80))
    date = db.Column(db.String(80))
    image = db.Column(db.String(80))
    description = db.Column(db.String())
    publisher_name = db.Column(db.String(80))
    pub_description = db.Column(db.String())
    parent_company = db.Column(db.String())
    website = db.Column(db.String())
    wiki = db.Column(db.String())
    author_name = db.Column(db.String(80))
    birthday = db.Column(db.String())
    nationality = db.Column(db.String())
    alma_mater = db.Column(db.String())
    auth_description = db.Column(db.String())
    image = db.Column(db.String())
    pub_image = db.Column(db.String())

db.drop_all()
db.create_all()
