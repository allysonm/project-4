import json
from models import app, db, Book

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()
    return jsn

def create_books():
    book = load_json('books.json')
    for oneBook in book:
        id = oneBook['google_id']
        title = oneBook['title']
        isbn = oneBook.get('isbn')
        date = oneBook.get('publication_date')
        image = oneBook.get('image_url')
        description = oneBook.get('description')
        for pub in oneBook.get('publishers'):
            publisher_name = pub.get('name')
            pub_description = pub.get('description')
            parent_company = pub.get('parent company')
            website = pub.get('website')
            wiki = pub.get('wikipedia_url')
            pub_image = pub.get('image_url')
        for auth in oneBook.get('authors'):
            author_name = auth.get('name')
            birthday = auth.get('born')
            nationality = auth.get('nationality')
            alma_mater = auth.get('alma_mater')
            auth_description = auth.get('description')
            image = auth.get('image_url')


        newBook = Book(title = title, id = id, isbn = isbn, date = date, description = description, publisher_name = publisher_name, author_name = author_name, pub_description = pub_description, parent_company = parent_company, website = website, wiki = wiki, birthday = birthday, nationality = nationality, alma_mater = alma_mater, auth_description = auth_description, image = image, pub_image = pub_image)

        # After I create the book, I can then add it to my session.
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()

create_books()
