from flask import Flask, render_template, request
from create_db import app, db, Book, create_books
from models import Book
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
import subprocess
from sqlalchemy import or_, func

@app.route('/')
def index():
	return render_template('static.html')

@app.route('/books/')
def books():
	books = db.session.query(Book).all()
	return render_template('all_titles.html', books = books)

@app.route('/title/<title>/')
def book_page(title):
	books = db.session.query(Book).all()
	for i in books:
		if i.title == title:
			author_name = i.author_name
			date = i.date
			isbn = i.isbn
			publisher_name = i.publisher_name
			description = i.description

	return render_template('book1.html', books = books, title = title, author_name = author_name, date = date, isbn = isbn, publisher_name = publisher_name, description = description)

@app.route('/author/<author_name>/')
def author_page(author_name):
	books = db.session.query(Book).all()
	booksPubs = []
	for i in books:
		if i.author_name == author_name:
			birthday = i.birthday
			nationality = i.nationality
			alma_mater = i.alma_mater
			auth_description = i.auth_description
			image = i.image
			booksPubs.append([i.title, i.publisher_name, i.date])
	return render_template('author1.html', books = books, booksPubs = booksPubs, author_name = author_name, birthday= birthday, nationality = nationality, alma_mater = alma_mater, auth_description = auth_description, image = image)

@app.route('/authors/')
def authors():
	books = db.session.query(Book).all()
	return render_template('authors.html', books = books)

@app.route('/publishers/')
def publishers():
	books = db.session.query(Book).all()
	return render_template('publishers.html', books = books)

@app.route('/publisher/<publisher_name>')
def publisher_page(publisher_name):
	books = db.session.query(Book).all()
	booksAuths = []
	for i in books:
		if i.publisher_name == publisher_name:
			pub_description = i.pub_description
			parent_company = i.parent_company
			website = i.website
			wiki = i.wiki
			pub_image = i.pub_image
			booksAuths.append([i.title, i.author_name, i.date])
	return render_template('publisher1.html', books = books, booksAuths = booksAuths, publisher_name = publisher_name, pub_description = pub_description, pub_image = pub_image, parent_company = parent_company, wiki = wiki, website = website)

@app.route('/about/')
def about():
	return render_template('about.html')

@app.route('/test/')
def test():
    p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE)
    out, err = p.communicate()
    output=err+out
    output = output.decode("utf-8") #convert from byte type to string type

    return render_template('test.html', output = "<br/>".join(output.split("\n")))

@app.route('/search/', methods=['POST'])
def search():
	search_prune = request.form
	search_prune=search_prune['search']
	str1=search_prune
	str="%"+search_prune+"%"

	search = db.session.query(Book).filter(or_(Book.title.ilike(str), Book.isbn.ilike(str), Book.date.ilike(str), Book.description.ilike(str), Book.id.ilike(str), Book.author_name.ilike(str), Book.birthday.ilike(str), Book.nationality.ilike(str), Book.alma_mater.ilike(str), Book.auth_description.ilike(str), Book.image.ilike(str), Book.publisher_name.ilike(str), Book.wiki.ilike(str), Book.pub_description.ilike(str), Book.parent_company.ilike(str), Book.pub_image.ilike(str), Book.website.ilike(str))).all()

	return render_template('search.html', search = search, str1=str1)


if __name__ == "__main__":
	app.run()
