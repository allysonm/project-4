import os
import sys
import unittest
from models import db, Book
from create_db import create_books


class DBTestCases(unittest.TestCase):

	def test_book_1(self):
		r = db.session.query(Book).filter_by(isbn = '9780062268372').one()
		self.assertEqual(str(r.isbn), '9780062268372')
		self.assertEqual(str(r.title), 'Yes Please')
		self.assertEqual(str(r.author_name), 'Amy Poehler')
		self.assertEqual(str(r.date), '2014-10-28')
		self.assertEqual(str(r.publisher_name), "HarperCollins")
		self.assertEqual(str(r.description), "#1 NEW YORK TIMES BESTSELLER Do you want to get to know the woman we first came to love on Comedy Central's Upright Citizens Brigade? Do you want to spend some time with the lady who made you howl with laughter on Saturday Night Live, and in movies like Baby Mama, Blades of Glory, and They Came Together? Do you find yourself daydreaming about hanging out with the actor behind the brilliant Leslie Knope on Parks and Recreation? Did you wish you were in the audience at the last two Golden Globes ceremonies, so you could bask in the hilarity of Amy's one-liners? If your answer to these questions is \"Yes Please!\" then you are in luck. In her first book, one of our most beloved funny folk delivers a smart, pointed, and ultimately inspirational read. Full of the comedic skill that makes us all love Amy, Yes Please is a rich and varied collection of stories, lists, poetry (Plastic Surgery Haiku, to be specific), photographs, mantras and advice. With chapters like \"Treat Your Career Like a Bad Boyfriend,\" \"Plain Girl Versus the Demon\" and \"The Robots Will Kill Us All\" Yes Please will make you think as much as it will make you laugh. Honest, personal, real, and righteous, Yes Please is full of words to live by.")

	def test_book_2(self):
		r = db.session.query(Book).filter_by(isbn = '1429992808').one()
		self.assertEqual(str(r.isbn), '1429992808')
		self.assertEqual(str(r.title), 'The Way of Kings')
		self.assertEqual(str(r.author_name), 'Brandon Sanderson')
		self.assertEqual(str(r.date), '2010-08-31')
		self.assertEqual(str(r.publisher_name), "Palgrave Macmillan")
		self.assertEqual(str(r.description), "From #1 New York Times bestselling author Brandon Sanderson, The Way of Kings, Book One of the Stormlight Archive begins an incredible new saga of epic proportion. Roshar is a world of stone and storms. Uncanny tempests of incredible power sweep across the rocky terrain so frequently that they have shaped ecology and civilization alike. Animals hide in shells, trees pull in branches, and grass retracts into the soilless ground. Cities are built only where the topography offers shelter. It has been centuries since the fall of the ten consecrated orders known as the Knights Radiant, but their Shardblades and Shardplate remain: mystical swords and suits of armor that transform ordinary men into near-invincible warriors. Men trade kingdoms for Shardblades. Wars were fought for them, and won by them. One such war rages on a ruined landscape called the Shattered Plains. There, Kaladin, who traded his medical apprenticeship for a spear to protect his little brother, has been reduced to slavery. In a war that makes no sense, where ten armies fight separately against a single foe, he struggles to save his men and to fathom the leaders who consider them expendable. Brightlord Dalinar Kholin commands one of those other armies. Like his brother, the late king, he is fascinated by an ancient text called The Way of Kings. Troubled by over-powering visions of ancient times and the Knights Radiant, he has begun to doubt his own sanity. Across the ocean, an untried young woman named Shallan seeks to train under an eminent scholar and notorious heretic, Dalinar's niece, Jasnah. Though she genuinely loves learning, Shallan's motives are less than pure. As she plans a daring theft, her research for Jasnah hints at secrets of the Knights Radiant and the true cause of the war. The result of over ten years of planning, writing, and world-building, The Way of Kings is but the opening movement of the Stormlight Archive, a bold masterpiece in the making. Speak again the ancient oaths: Life before death. Strength before weakness. Journey before Destination. and return to men the Shards they once bore. The Knights Radiant must stand again. The Cosmere The Mistborn series Mistborn: The Final Empire The Well of Ascension The Hero of Ages Alloy of Law Shadows of Self Bands of Mourning The Stormlight Archive The Way of Kings Words of Radiance Edgedancer (Novella) Oathbringer (forthcoming) Collection Arcanum Unbounded Other Cosmere Titles Elantris Warbreaker Rithmatist The Alcatraz vs. the Evil Librarians series Alcatraz vs. the Evil Librarians The Scrivener's Bones The Knights of Crystallia The Shattered Lens The Dark Talent The Reckoners Steelheart Firefight Calamity At the Publisher's request, this title is being sold without Digital Rights Management Software (DRM) applied.")

	def test_book_3(self):
		r = db.session.query(Book).filter_by(isbn = '9781439170915').one()
		self.assertEqual(str(r.isbn), '9781439170915')
		self.assertEqual(str(r.title), 'The Emperor of All Maladies')
		self.assertEqual(str(r.author_name), 'Siddhartha Mukherjee')
		self.assertEqual(str(r.date), '2011-08-09')
		self.assertEqual(str(r.publisher_name), "Simon & Schuster")
		self.assertEqual(str(r.description), "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.")

	def test_author_1(self):
		r = db.session.query(Book).filter_by(author_name = 'Richard Rhodes').first()
		self.assertEqual(str(r.author_name), 'Richard Rhodes')
		self.assertEqual(str(r.birthday), '1937-07-04')
		self.assertEqual(str(r.nationality), 'American')
		self.assertEqual(str(r.alma_mater), 'Yale University')
		self.assertEqual(str(r.auth_description), "Richard Lee Rhodes is an American historian, journalist and author of both fiction and non-fiction, including the Pulitzer Prize-winning The Making of the Atomic Bomb, and most recently, The Twilight of the Bombs.\n")

	def test_author_2(self):
		r = db.session.query(Book).filter_by(author_name = 'J. K. Rowling').first()
		self.assertEqual(str(r.author_name), 'J. K. Rowling')
		self.assertEqual(str(r.birthday), '1965-07-31')
		self.assertEqual(str(r.nationality), 'British')
		self.assertEqual(str(r.alma_mater), 'University of Exeter')
		self.assertEqual(str(r.auth_description), "Joanne \"Jo\" Rowling, OBE, FRSL, pen names J. K. Rowling and Robert Galbraith, is a British novelist, screenwriter and film producer best known as the author of the Harry Potter fantasy series. ")

	def test_author_3(self):
		r = db.session.query(Book).filter_by(author_name = 'Brandon Sanderson').first()
		self.assertEqual(str(r.author_name), 'Brandon Sanderson')
		self.assertEqual(str(r.birthday), '1975-12-19')
		self.assertEqual(str(r.nationality), 'American')
		self.assertEqual(str(r.alma_mater), 'Brigham Young University (B.A., M.A.)')
		self.assertEqual(str(r.auth_description), "Brandon Sanderson is an American fantasy and science fiction writer. He is best known for his Mistborn series and his work in finishing Robert Jordan's epic fantasy series The Wheel of Time. ")

	def test_publisher_1(self):
		r = db.session.query(Book).filter_by(publisher_name = 'Pottermore').first()
		self.assertEqual(str(r.publisher_name), 'Pottermore')
		self.assertEqual(str(r.parent_company), 'None')
		self.assertEqual(str(r.website), 'http://www.pottermore.com\nshop.pottermore.com')
		self.assertEqual(str(r.wiki), 'https://en.wikipedia.org/wiki/Pottermore')
		self.assertEqual(str(r.pub_description), "Pottermore is the digital publishing, e-commerce, entertainment, and news company from J.K. Rowling and is the global digital publisher of Harry Potter and J.K. Rowling's Wizarding World.")

	def test_publisher_2(self):
		r = db.session.query(Book).filter_by(publisher_name = 'Scholastic Corporation').first()
		self.assertEqual(str(r.publisher_name), 'Scholastic Corporation')
		self.assertEqual(str(r.parent_company), 'None')
		self.assertEqual(str(r.website), 'http://www.scholastic.com')
		self.assertEqual(str(r.wiki), 'https://en.wikipedia.org/wiki/Scholastic_Corporation')
		self.assertEqual(str(r.pub_description), "Scholastic Corporation is an American publishing, education and media company known for publishing, selling, and distributing books and educational materials for schools, teachers, parents, and children. ")

	def test_publisher_3(self):
		r = db.session.query(Book).filter_by(publisher_name = 'Bloomsbury Publishing').first()
		self.assertEqual(str(r.publisher_name), 'Bloomsbury Publishing')
		self.assertEqual(str(r.parent_company), 'None')
		self.assertEqual(str(r.website), 'http://Bloomsbury')
		self.assertEqual(str(r.wiki), 'https://en.wikipedia.org/wiki/Bloomsbury_Publishing')
		self.assertEqual(str(r.pub_description), "Bloomsbury Publishing Plc is a British independent, worldwide publishing house of fiction and non-fiction. It is a constituent of the FTSE SmallCap Index. ")
	
	def test_search_author(self):
		r = db.session.query(Book).filter_by(author_name = 'J. R. R. Tolkien').all()
		self.assertEqual(len(r), 4)
		self.assertEqual(str(r[0].birthday), '1892-01-03')
		self.assertEqual(str(r[0].author_name), 'J. R. R. Tolkien')
		self.assertEqual(str(r[0].title), 'The Fellowship of the Ring')
		self.assertEqual(str(r[1].author_name), 'J. R. R. Tolkien')
		self.assertEqual(str(r[1].title), 'The Two Towers')
		self.assertEqual(str(r[2].author_name), 'J. R. R. Tolkien')
		self.assertEqual(str(r[2].title), 'The Return of the King')
		self.assertEqual(str(r[3].author_name), 'J. R. R. Tolkien')
		self.assertEqual(str(r[3].title), 'The Hobbit')
		
	def test_search_book(self):
		r = db.session.query(Book).filter_by(title = '1984').all()
		self.assertEqual(len(r), 1)
		self.assertEqual(str(r[0].title), '1984')
		self.assertEqual(str(r[0].isbn), '9781784043735')
		self.assertEqual(str(r[0].author_name), 'George Orwell')
		self.assertEqual(str(r[0].publisher_name), 'Arcturus Publishing')
	
	def test_search_publisher(self):
		r = db.session.query(Book).filter_by(publisher_name = 'Scholastic Corporation').all()
		self.assertEqual(len(r), 3)
		self.assertEqual(str(r[0].publisher_name), 'Scholastic Corporation')
		self.assertEqual(str(r[0].title), 'Harry Potter and the Order of the Phoenix')
		self.assertEqual(str(r[0].author_name), 'J. K. Rowling')
		self.assertEqual(str(r[1].publisher_name), 'Scholastic Corporation')
		self.assertEqual(str(r[1].title), 'The Old Man and the Sea')
		self.assertEqual(str(r[1].author_name), 'Ernest Hemingway')
		self.assertEqual(str(r[2].publisher_name), 'Scholastic Corporation')
		self.assertEqual(str(r[2].title), 'Animal Farm')
		self.assertEqual(str(r[2].author_name), 'George Orwell')
		

if __name__ == '__main__':
    unittest.main()
